# Source code of Wuzzy’s Personal Website

This is the source code for Wuzzy’s Personal Website.

For the actual website, go to: <https://wuzzy.codeberg.page>

For the Codeberg Pages repository, go to: <https://codeberg.org/Wuzzy/pages>

## How to build

This is a static website using [Hugo](https://gohugo.io) to be built. So you need Hugo to build this.

To build, just type this into the console:

    hugo

Then retrieve the result from the directory `public`.
This directory contains all the webpages and data files and can be uploaded to a webserver.

If you want to build the site again, delete `public` first.

## License

Everything is under CC0 (Public Domain Dedication), unless otherwise noted.

The Wuzzy logo is under CC0.

Other logos and screenshots might fall under different conditions, see
`content/meta/copying.md` for details.
