---
title: "On asexuality and aromanticism"
date: 2023-03-19T18:53:00+01:00
showdate: true
usestoc: true
---

## Introduction

This is an essay in which I want to talk about the concepts of asexuality and aromanticism. I believe awareness about these things is kinda important and we should not ignore them.

## Summary

Here are my points, summarized:

* **Not everyone feels sexual and/or romantic attraction**
* **Asexuality** is when you feel little or no sexual attraction
* **Aromanticism** is when you feel little or no romantic attraction
* Aromantic and asexual people exist and are valid
* The false belief that *every* adult feels romantic and sexual attraction—**allonormativity**—is harmful
* We should educate more people about the **truth** behind these identities. Education is a good in its own right

## About me

I am a sex-averse romance-repulsed asexual aromantic person (these words will be explained later). I have also talked and listend to a lot of other members of the asexual and aromantic communities.

I think I’m more than qualified to talk about this topic. To learn more about my identity, see [About me](/about/me).

## What is asexuality and aromanticism?
### Explanation

Let’s start with the facts:

<img class="logo" src="/assets/images/asexual_flag.png" width="150" height="90" alt="The asexual flag. 4 horizontal stripes, from top to bottom: black, gray, white, purple" title="Asexual Pride Flag">

There exists a minority of people who do experience no or very little sexual attraction. These people are known as **asexual**.

<br class="imgclear">

<img class="logo" src="/assets/images/aromantic_flag.png" width="150" height="90" alt="The aromantic flag. 5 horizontal stripes, from top to bottom: green, light green, white, gray, black" title="Aromantic Pride Flag">

Additionally, there also exists a minority of people who experience no or very little romantic attraction. These people are known as **aromantic**.

<br class="imgclear">

Finally, there are also people who fall somewhere on the asexual and/or aromantic spectrum. These people are very diverse. No two asexual people are alike, and there are many ways how one could experience asexuality. The same is true for aromanticism.

As for me, I am both asexual and aromantic, or **aro-ace** in short.

### Debunking gut reactions

When you first hear about asexuality and/or aromanticism, you might be inclined to have a certain gut reaction.

#### “This sounds so sad!”

If your first thought is something like “Oh my Gosh! This is soo *sad* that you are unable to get laid!”, then trust me: You are not the first one who had this thought. In fact, yours truly had that thought as well, and I’m a raging asexual.

This is a *very* common first reaction. But it turns out, being an aro-ace really isn’t that big of a deal. I don’t feel sad. It’s more like a “whatever” attitude for me. Many aces and aros are like that.

Are there some aces and aros who are sad? Sure, just like in any other group. But are they sad *because* the are ace or aro? I don’t think so. I have *never* heard of such a person existing.

#### “You just haven’t found the right person yet!”

But what if there is no right person? This is how many aces and aros feel, me included. And many aces and aros, in fact, *do* feel happy without a sexual/romantic relationship. If you feel no sexual or romantic attraction towards anyone, then *by definition* there can’t be a right romantic or sexual partner.

That having said: Some aces and aros still might have a life partner (or multiple), but it’s not about sex and/or romance. Friendships exist. It can be very dismissive and arrogant to imply that a *friend* can’t be a “right person”.

See also [Nose Ears #554](https://wuzzy.neocities.org/comic/554/).

#### “But what *if* you suddenly find the perfect partner and you fall in love, have lots of sex, bring children into the world and live happily ever after?”

From the viewpoint of an ace or aro, this is a game you can’t win.

Note how hypothetical this scenario is: So there *might* be a magical surprise partner in the future, something that is basically impossible to predict. This is already the first problem: It’s a practically unfalsifiable claim because how can you prove there will *not* a magical surprise partner in future? You can only do that by living your life and then die. That’s not an useful way to view life. In fact, it’s actively *harmful*.

It gets worse: What if you are asexual and/or aromantic, but you’re not sure about your identity so you take this question seriously. The magical surprise partner will likely never come, but you stumble through life, waiting for the One True Partner although they *can’t* exist. This is a guaranteed way to feel miserable.

The rational response to this question is basically a “So what?”. No matter what happens, no matter your identity, waiting for a surprise partner is never useful.

See also [Nose Ears #190](https://wuzzy.neocities.org/comic/190/).

#### “You just invent new words for no reason.”

All of the new words do have an actual meaning. The reason why there are so many words is because humans are just complex. There are many aspects of sexual and romantic orientation which makes it absolutely complex. If reality was simpler, we had simpler language. But the existence of aces an aros means the language needs to accommodate for that in some way.

## Allonormativity 101

All the gut reactions above have something in common: They all hinge on a deeply held belief. If you understand where these beliefs are coming from, it should be obvious why these are wrong.

There is a widespread but false superstition that everyone, yes, literally everyone *will* eventually feel sexually and romantically attracted to someone at some point. This belief is so deeply ingrained that most people don’t even realize they have that belief. It’s just a *default* assumption that many people have.

This belief is called “**allonormativity**”.

**Allonormativity is false.** There is zero evidence that allonormativity is true. The opposite is true: **There are *millions* of people who are asexual or aromantic.** To believe in allonormativity is to deny that reality.

### Why allonormativity sucks

Here we get to the heart of my essay: Why I believe talking about being ace and aro is important.

The reason is **allonormativity**.

Even people who are more or less positive to lesbians, gays and bisexuals (or are those people themselves) can be dismissive of the very *existence* of aces and aros. Why is that? The reason is simple: Homo- and bisexuality are sexual orientations that are rejected because they are not heterosexual.

The main reason why people reject homosexuality and bisexuality is because of **heteronormativity**, the belief that everyone is or should be straight. But if you reject heteronormativity, you can’t also reject the validity of homo- and bisexuality. The reverse is also true: If you embrace heteronormativity (which you shouldn’t), you can’t be accepting of homo- and bisexuals. You can’t have it both ways.

Let’s say you reject heteronormativity and you accept homo- and bisexuality. Cool. This means you now accept that people can feel attracted to people regardless of gender. Same gender, other gender, multiple genders, it does not matter.

However, it is possible for a person to reject heteronormativity and to accept homo- and bisexuality but to *not* accept asexuality. This might seem paradox at first. Because homosexuality, bisexuality and asexuality are all *not* heterosexuality, thus they are incompatible with heteronormativity. However, there’s something that heterosexuality, bisexuality and homosexuality have in common: You do feel *some* sexual attraction in all 3 cases. These orientations (hetero, bi, homo) fall under the umbrella of **allosexuality**.

**Allosexuality** means that you feel at least *some* sexual orientation, usually to a degree that is deemed normal. However, **asexuality throws a wrench into the machine.** It not just throws a wrench into heterosexuality but *also* to allosexuality. Homo- and bisexuality do *not* do that.

This explains why even people who *believe* they are more or less pro-<abbr title="Lesbian, Gay, Bisexual, Trans*, Queer, Questioning, Intersex, Asexual, Aromantic, Agender, Pansexual, others">LGBTQIAP+</abbr> might still dismiss the concept of asexuality as invalid. You could be the best gay, lesbian, bisexual ally in the world but still dismiss aces and aros. Why? Because of **allonormativity**. The reality is, aces and aros are *real*. If you want to seriously and honestly defend allonormativity, you have to somehow justify how all the millions of people who *are* asexual and/or aromantic do not exist, or are all delusional. Yeah, good luck with that.

In short, I think **rejecting heteronormativity is not enough**. To overcome bigotry, you also have to explicitly **reject allonormativity** as well.

### How allonormativity screwed me over

It **took me fairly long** to come to accept the fact I am asexual. For decades, I just assumed I was straight although I didn’t realize I never felt any sexual or romantic attraction, ever. Like, zero. There was absolutely nothing. Yet somehow I assumed it will just magically happen. At least for sex. It never happened.

Now I know **I’m aro-ace and it all makes perfect sense now**. And I’m glad we have this thing called the “Internet”.

**I bought into heteronormativity *and* allonormativity** hook, line and sinker. Would asexuality and aromanticism have been more visible in culture overall, I might have accepted my true self much earlier.

While I wasn’t ever depressed for deluding myself into believing I’m straight, it wasn’t great either. I was getting nervous I wasn’t getting laid, as if that was a serious problem. Turns out, no, it wasn’t a problem at all. I just believed into the delusion of society that everyone wants and/or needs sex.

## Knowledge matters

For the reasons above, I believe education and awareness about these topics matters a lot.

First, education is a good thing in its own right, always.

But it also serves a very direct goal: It will possibly help *other* aces, aros and anyone else in the spectrum to find themselves. I know increased awareness would definitely helped *me* and I could really have used this knowledge a few years earlier.

Many aces and aros run around the world and wonder why they never feel attraction, and they feel broken. This is a story way too common in the ace and aro communities. What they don’t realize is that there’s nothing wrong for not feeling attraction, it’s just who you are. As I’ve explained above, allonormativity can be a nasty beast, so it has to be fought.

## Closing words

There is so much more to learn about aces and aros. Like, the asexual and aromantic spectra, gray-asexuality, demiromantic people, sex-repulsion, and so much more. This essay was just an introduction. But I stop here. Just one more thing: No two aces and/or aros are alike. We’re actually a very diverse community. I know this because I’ve talked to a *lot* of them.

If you want to learn more about asexuality in general, hop over to [AVENwiki](http://wiki.asexuality.org/Main_Page). For aromanticism, check out the [AUREA FAQ](https://www.aromanticism.org/en/faq).

## Appendix
### Definitions

* **Asexual**: When you’re not experiencing any or only very little sexual attraction to people
* **Ace**: Short for “asexual”
* **Sex-averse**: When you feel averse to the idea of being directly involved in sexual activities with other people. Does not necessarily mean you are disturbed by sexual activity of *other* people
* **Romance-repulsed**: When you feel strongly repulsed by the idea of being involved in any kind of romance and just can’t stand it. You want to be removed from romance as far as possible
* **Aromantic**: When you’re not experiencing any or only very little romantic attraction to people
* **Aro**: Short for “aromantic”
* **Allonormativity**: The *false* belief that everyone (or at least every adult) will feel sexual and romantic attraction at some point
* **Heteronormativity**: The *false* belief that everyone (or at least every adult) is straight (heterosexual and heteroromantic)
* **Heterosexual**: When you feel sexually attracted to people of a gender other than your own
* **Heteroromantic**: When you feel romantically attracted to people of a gender other than your own. Basically the romantic equivalent to heterosexuality
