---
title: "How I (and others) liberated the sounds of OpenTTD"
date: 2023-04-24
showdate: true
---
## Introduction
**Note**: The text of this essay is based on a [FreeGameDev forum post](https://forum.freegamedev.net/viewtopic.php?f=6&t=18150&p=103366) I wrote in 3 July 2021.

This is an essay about the unglamorous yet important part of free software game development: Media licensing.

In particular, this is about how OpenTTD’s default sound pack, OpenSFX, accidentally was distributed with non-free sound files for over 10 years, and how this was eventually fixed because I worked on it.

[OpenTTD](https://openttd.org) is a free software transportation sim game. The goal of this project is to create a free/libre replacement for a much older (and proprietary) game, TransportTycoon Deluxe. Since the release of version 1.0, OpenTTD gained quite some popularity in the free software world and beyond.

## The problem
So, OpenTTD itself is free software, there is no doubt about it. But there’s a catch: It turned out the official sound pack of OpenTTD, named “[OpenSFX](https://www.openttd.org/downloads/opensfx-releases/latest)”, was actually not libre (that is, under a license compatible with free culture standards).

The problem with OpenSFX was that it was licensed under the archaic non-libre [CC Sampling Plus 1.0](https://creativecommons.org/licenses/sampling+/1.0/) license. It’s not libre because it essentially forbids most forms advertisement (even non-commercial!) and there are a lot of other problems. To quote the license summary:

> You may not use this work to advertise for or promote anything but the work you create from it. 

This is incompatible with free culture ideals. That’s not the spirit of free software or free culture. People should be free to use the thing for anything they like. Restricting advertisement is clearly unfair.

This ancient license is so bad, even [Creative Commons](https://creativecommons.org) themselves [retired](https://creativecommons.org/2011/09/12/celebrating-freesound-2-0-retiring-sampling-licenses/) it in 2011. They no longer promote this license, they actively discourage people from using it.

Choosing Sampling Plus for OpenSFX was likely an accident, and the original OpenSFX authors from over a decade ago didn’t know better. OpenSFX is very, very old, and it was created when Creative Commons was still relatively young.

Note that OpenTTD and OpenSFX are clearly separated; OpenTTD has always worked without OpenSFX. So *technically*, OpenTTD was still free software even without OpenSFX, because players didn’t have to install it. However, in practice, a libre game without libre sounds is kind of missing the point. I believe libre games need to go all the way.

## The liberation

Anyway, since this has bothered me for so long, I have finally decided to do something about it, and worked on contacting authors and editors to re-license this. And also remaking a couple of sounds for which this is not possible.
It turned out most sounds were based on sounds from [freesound.org](https://freesound.org). And thankfully, a majority of the source sounds were under libre licenses already, they were just listed (incorrectly) under the Sampling Plus license in OpenSFX.

But there was another hurdle: The editors. These were OpenTTD contributors who took the sounds (mostly) from Freesound.org and edited them for better gameplay. And their work was collectively Sampling Plus’ed. So I also needed to collect permissions from those. Unfortunately, most people have already left, I believe OpenSFX was created ca. 10 years ago. But I got permission from a few people, and so I could easily re-license.

First, the responses I got from some people were pretty frustrating. I was told not much about the origin of the sounds was known. If that were true, the only way forward would be to replace all 73 sounds.
Thankfully, later I found out that the sound origins were actually documented very well, it was a file in an awkward location. This was very helpful later on.

So the situation was this: There were a couple of files based on non-free sources, and for which my attempts to get permission from the authors have failed. So those sounds needed to be replaced no matter what.
And then were also sounds based on libre source sounds, but they were still Sampling Plus’ed because that’s what the editors chose.
Both hurdles have to be overcome to solve this.

What I did was the following:

* Researched the origin of all sounds (license and freesound.org URL)
* Contact almost everyone listed in the credits in the hopes to get permission to re-license (mostly with private messages on freesound.org)
* Waited, and got a few permissions (yay!) and re-licensed sounds (not more than 5 in total), but only from a minority
* Did nothing for ca. 1.5 years because I got bored, lol
* Started again, then sorted all sounds by license for better overview
* Contacted the editors again, got permission from 2 (out of 5)
* Replaced ALL sounds from non-libre sources (this required me to use completely new sounds, I used freesound.org and also [OpenGameArt](https://opengameart.org/) one time. While I was at replacing, I tried to also improve their quality while I was at it. :-) )
* Because of my own editing work based on libre sources, those sounds were automatically libre. Done!
* With the permission from the editors of sounds from libre sources, I was able to flip the license of many other sounds
* Contributed some non-sound-related edits to OpenSFX, mainly to make the old and clunky build system functional again. The main developers also started to work at the OpenSFX build system again, which was nice
* Nagged the OpenTTD developers for weeks to finally make a new release for OpenSFX

The actual sound replacement finding and editing took me about 2 weeks on-and-off. The technical cleanup afterwards and waiting took a little longer.

And finally, I have created a libre version of OpenSFX, but some other people helped me as well. Most obviously, the people who uploaded sounds, but also the main OpenTTD developers. We also had to do quite a lot of cleanup of the build system (which is necessary to make releases), this was years-old dusty code that no longer worked which definitely needed an overhaul. And I actually had some fun with all the sound editing, it was an interesting experience. :-)

### The first libre release

OpenSFX version 1.0.0 has been released around March 2021. This is the first version that is actually libre, after it has been non-free for over 10 years.

Since version 1.0.0, OpenSFX is under [Creative Commons Attribution-ShareAlike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/), a well-known free culture license.

### Lessons learned

* Copyright *sucks*. The fact that the sounds didn’t just enter the Public Domain automatically after over 10 years is very frustrating. That’s an *eternity* in Internet time
* You actually *can* make non-free data files legally free/libre by just asking, but the chances are against you. However, this still might save you *some* work. If you ask literally everyone, you will likely convince at least a few people
* Better get licensing right from the start and insist on freedom early on
* Document all the data sources while you still can (if there would have been no documentation on the sound sources, this would have been even more annoying, as I would have needed to start from zero)

## Closing

I wrote this essay as a cautionary tale for all free software and free culture enthusiasts that it’s pretty important to get the licensing right at an early stage. ;-) Oh, and also because I feel some pride in my work. The new sounds are pretty good, too. :-D

## Appendix

Here are threads on [tt-forums.net](https://www.tt-forums.net) for those who want to read more:

* <https://www.tt-forums.net/viewtopic.php?f=29&t=44761> (original OpenSFX thread, it’s very old, it gets interesting at the end only)
* <https://www.tt-forums.net/viewtopic.php?f=29&t=85815> (my thread in which I first complained about OpenSFX being non-free)
