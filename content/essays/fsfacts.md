---
title: "Free Software Facts"
date: 2023-03-20T03:11:00+01:00
showdate: true
usestoc: true
---
## Introduction

Public knowledge about the concept of free software is still limited. While some might heard the term before, many people don’t really understand it. Even developers get it wrong sometimes. I have heard many myths over and over again, so in this essay I counter these myths with some facts.

## Fact: Free software is software that respects user’s essential freedoms

Before we get started, let’s get the basics right. According to the [Free Software Definition](https://www.gnu.org/philosophy/free-sw), any given software is a *free software* if and only if *all* of these 4 freedoms are guaranteed:

> * The freedom to run the program as you wish, for any purpose (freedom 0).
> * The freedom to study how the program works, and change it so it does your computing as you wish (freedom 1). Access to the source code is a precondition for this.
> * The freedom to redistribute copies so you can help others (freedom 2).
> * The freedom to distribute copies of your modified versions to others (freedom 3). By doing this you can give the whole community a chance to benefit from your changes. Access to the source code is a precondition for this.

These are also known as the “four freedoms”.

The reason why this definition exists in the first place is that in our current culture, the default legal assumption is that users and developers *do not* possess *any* of these freedoms, usually thanks to copyright laws. For a non-free software, you can (and often will) get punished by the state should you choose to exercise any of these freedoms.

Any software that is not free software is called “non-free software” or “proprietary software”.

## Fact: Free software is not about price

Free software is about the user having the four freedoms (see above). The *price* of the software is simply a separate question. A free software may or may not cost nothing. It depends on whoever is redistributing the software.

What you can *not* do with free software is to declare a monopoly on it and forbid anyone else from selling copies.

*In practice*, most free software is gratis.

## Fact: Free software is not the same as freeware

With “freeware”, people usually mean software that costs nothing. Under this definition, a freeware might or might not be *free software* as well. However, *other* people define “freeware” as any *proprietary* software that costs nothing. Under this definition, a freeware can never be free software.

The problem with the word “freeware” is that there isn’t really a standard definition for it so it should be used with care.

In any case, “freeware” should *never* be used as a *synonym* for free software and vice-versa. That would be factually incorrect. Free software is about freedom, while freeware is about something else. The distinction matters.

As a personal request: Please refer to free software as “free software” and not “freeware”, even if it is gratis.

## Fact: Not all free software projects are collaborative

A large chunk of free software is actually developed without any collaboration.

However, there is a widespread myth that free software projects are *always* collaborative. This is false.

Sure, there are a few well-known large software projects with a large collaboration effort behind: the GIMP, Blender, CMake, Inkscape, The Battle for Wesnoth, etc. And for these softwares, collaboration is very important.

But there is also a *huge* amount of small or tiny software projects with only a very few or even just one developer. Especially very small projects might not be willing to accept patches or contributions from anyone. Yet they still qualify as free software. What makes a software free software is when the users have their four freedoms intact.

The big well-known free software projects tend to be collaborative (which is probably why this myth exists in the first place). But a lack in collaboration isn't necessarily a bad thing. It depends on a case-by-case basis. I know of many great softwares which only have one developer.

Whether a software is free or non-free, and whether a software is created collaboratively or alone, these are two completely separate questions.

## Fact: Many people contribute to free software, not just developers

Free software, like any software, is often more than just code. Programming is only part of it. You often also need documentation writers, testers, translators, maintainers, for example.

The importance of non-programmers obvious when it comes to computer games: You also need artists, musicians, sound designers, level designers, story writers and many other people who might not write a single line of code. All these people are *critically* important for the success of a game. Without artists, you have no graphics. With out level designers, you have no levels, and so on.

Just because you’re not a programmer doesn’t need you can’t contribute to free software.

## Fact: The term “free software” is still relevant today

There’s a myth among those who advocate for the term “open source”. They claim the term “free software” is actually just an outdated term for “open source”. This is false.

The term “free software” is of course still relevant and in use today. The term “open source” never was an “updated term”, it was created as a *competing term* with the goal to *replace* the term “free software”. Well, that didn’t happen.

According to the [Jargon File](http://www.catb.org/jargon/html/O/open-source.html), the reason why “open source” was coined was a strategic one: To trick corporations into accepting free software without *calling* it free software. While many corporations might have happily adopted the *term* open source, not all have adopted the *spirit* of open source. And in fact, there is widespread abuse of the term.

When a corporation says “open source” today, they often mean something that actually has nothing to do with open source as originally intended (let alone free software).

## Fact: Free software games can be programmed in a way to hold cheaters at bay

A very common myth is that free software games are inherently vulnerable to cheaters and therefore need to be proprietary. This is based on a fallacy called _security by obscurity_, the idea that by keeping the source code secret, you’re secure.

But this is simply delusional: If people have your software and they desperately *want* to cheat, then you can bet they will dissect your software. Not having access to source code won't stop them. Do not underestimate of what lengths some cheaters are willing to go. 

To make things worse, to create a successful cheat, hack, trainer, aimbot or whatever, it only takes one determined person to do so who will then share this cheat with the Internet. If you have the software on your computer and time, every deliberate obfuscation, copy protection, DRM measure, and the like will eventually crumble down.

If you’re a gamer, never buy into arguments from game publishers that their game *needs* to be proprietary because of cheaters. They’re either delusional or lying. It is perfectly possible to make a game robust against cheaters while also being free software.

What a robust game really needs is a security model which keeps cheaters at bay in the first place. The developer’s goal must be to make the game secure *even if cheaters know exactly how it works*. Anything else is “security by obscurity”, which is fake security.

It the game is poorly designed and exploitable, it *will* be exploited and the developers *will* be laughed at for their incompetence.

A game being proprietary does not add any extra security. The only thing it does is to remove freedom.

## Fact: Non-free software licenses that explicitly discriminate against a certain group of people exist

Many people wonder why freedom 0 exists in the Free Software Definition. That is, the freedom to use the software for any purpose. No developer would *actually* want to forbid people from *using* their software, right? Yet this thing actually happens. Discriminatory clauses have been written into licenses.

Discriminations that have actually occurred before include:

* Against people who live in the “wrong” nation (example: [Creative Commons Developing Nations License](https://creativecommons.org/licenses/devnations/2.0/))
* Against people in the military
* Against people who work for the government (directly or indirectly)
* Against people who “do evil” (whatever that means)

## Fact: The freedom to redistribute copies matters

If you redistribute copies without permission of the copyright holder, the state might come after you to punish you.

Some people have argued the freedom to redistribute is irrelevant because people already share copies of software—free and non-free—anyway all over the Internet. That’s a fallacy.

The freedom to redistribute copies would only be irrelevant in a society that knows no copyright or similar restrictions. However, in our society, copyright is still a thing. While you can *technically* share any software at any time with anyone online, it is only a question of time until you get sued and the authorities go after you.

As long the state enforces copyright, we must assume that people *do not* have the freedom to share copies with others by default.

## Fact: Not all free software projects are hobbyist projects

It is true that some free softwares are just hobbyist softwares. However, that’s nothing special about free software. You can find hobbyist software in the proprietary world as well. Many small freeware programs are hobbyist softwares.

But there is also a significant number of free softwares that have reached a level of complexity it would be a huge stretch to call them “hobbyist” software anymore. Is GIMP just a hobby software? MySQL? Python? The entire GNU Project?
