---
title: "GitHub is not free software"
date: 2023-03-20T02:18:00+01:00
showdate: true
---

I’m not sure everyone in the free software community has gotten the memo yet:

**GitHub is not free software!**

I need to explain that statement.

For people in the free software and the developer world in general, GitHub is very well-known.
GitHub surely *hosts* a lot of free software.

So it is easy to assume that GitHub is free software as well. Nothing could be further from the truth.

GitHub is not free software. There is a lot of proprietary code when you’re around GitHub.

GitHub suffers from what is called the “[JavaScript Trap](https://www.gnu.org/philosophy/javascript-trap.html)”. The thing that makes GitHub proprietary is the JavaScript embedded in the web pages. Most, if not all of these JavaScript contain obfuscated code. In simple words: If you open GitHub in your browser at default settings, you have already executed proprietary code.

You can, theoretically, turn off JavaScript, and still browse on GitHub. However, a few features will just be broken. Turning off JavaScript still kinda sorta works, but you have to deal with some bugs. It sucks.

## Why do I care?

If we want free software to truly win, we cannot tolerate non-free software. JavaScript is no exception. This is *especially* true when non-free software is deeply integrated in the development workflow. I strongly believe that the *development* of free software should also be done by 100% free software tools.

Integrating GitHub into the workflow means you’re tolerating non-free software. Worse, you teach new developers that non-free software is socially acceptable sometimes, *even* when it is an essential part of your workflow. This is not OK.

What frustrates me about GitHub the most is that *we don’t actually need GitHub*. Other code hosting sites with 100% free software already exist, we just need to use them.

## Moving forward

If you agree with me that GitHub is bad, what could you do?

* Move all your projects away from GitHub. I recommend to migrate to [Codeberg.org](https://codeberg.org).
* Talk to other software maintainers on GitHub to consider to migrate. The more people embrace free software all the way, the easier it will become for all of us.
* Talk publicly about the JavaScript Trap.
* If you can afford it, support free software code hosting services. The more stable their finances, the less the need to rely on a de-facto monopolist like GitHub
* Convince Microsoft to release the entirety of GitHub as free software (or at least the JavaScript code). Good luck. :D

### When migration could be difficult

If you have a small or even medium-sized software project, a site like Codeberg.org is more than enough. They have migration tools that are super easy to use. They import the repository, issues and pull requests and possibly some more.

However, if your software is currently deeply ingrained into GitHub, and you use a lot of its more advanced features, migration can be tricky. Make sure to check out the features of Codeberg.org (or wherever you migrate to) first.

You might also consider self-hosting.

If you absolutely can’t migrate away from GitHub (although you would love to), talk to the folks at Codeberg.org anyway. Maybe they have some hints.
