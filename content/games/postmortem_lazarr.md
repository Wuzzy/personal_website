---
title: "Post-mortem for Minetest Game Jam 2021"
date: 2023-03-24
showdate: true
---

I’ve participated in the Minetest Game Jam 2021 (note: Minetest is now “Luanti”) by submitting the game “Lazarr!”. It didn’t do well and only ranked #13 out of 25. My main problem is that everyone had a whole 3 weeks of time but I only used the final day.

This is my analysis of my game’s development for the game jam. I’ve originally [posted](https://forum.minetest.net/viewtopic.php?p=404948#p404948) the following text on the Minetest forums on January 2, 2022.

Note: The screwdriver mentioned below is what I later replaced with the hook in later versions. It’s to rotate the mirrors.

## Post-mortem

This was my first game jam ever and I am honestly surprised I managed to get out a (barely) playable game in the first place? Why? Because I literally had this game idea in the final day. It's a complete trainwreck of time-mismanagement.
I originally had a completely different idea for a game, namely some kind of arena battle in which you must survive attacks from monsters. But I procastinated (I started playing addictive video games ... The irony!) after the first day and on the 2nd-to-last day I realized my idea really doesn't work out and programming the mobs with proper behavior, battle mechanic, etc. with only 2 days left was no option. So I woke up the last day, completely spontanously had the idea for "something with lasers". I really wish I had this idea earlier, lol. There were only like 12 hours left and I crunshed like crazy just to get something out. I could have just given up and do nothing but I would probably have just kicked myself in the ass for ignoring the game jam completely, I \*had\* to submit \*somthing\* lol. I'm happy I managed to get it into a BARELY playable state, and I am not surprised by my poor rank, it was expected. So if you were disappointed by my game, you're right, it was the best I could do in only 12 hours. It was hard, but … an interesting experience, haha. So my "strategy" was to do the abolute minimal but playable thing I could reasonably do in the time, with almost zero polish, so the features also had to be as minimal as possible, the only thing that mattered to me at that point to be playable. :D
Many were annoyed by the screwdriver and I agree. I should probably have picked screwdriver2 or coded my own (but again: no time, ouch!).
And I also made a stupid mistake in that one of the 10 levels was literally unsolvable >\_> I noticed this hours after the submission ... Ooff!
I did not completely start over, SOME code from my "arena game" I was able to re-use, but not much (basically the basic architecture stuff).

As for the development in detail, it actually went surprisingly smoothly. I took like 1-2 hours for the laser nodes, a couple of hours for calculating and updating the playing field, another hour only for documenting the licensing/readme stuff ( >\_> ), 1 hour for levels, I guess like 1 hour for graphics and sound (and minifying xdecor), and the rest of the time for the rest. I got actually lucky while development since I didn't get stuck at anything, didn't run into a brickwall with a serious bug that had to figure out before I coud continue. If I would have hit any serious brickwall, I probably would not have reached playability at the deadline. It was close, VERY close. And at that day, I only made 1 short pause of like 30min, the rest of the day I only worked, did nothing else. Crunshing development is bad, y'all, take your time. :-)

Overall, I am totally not surprised by my poor ratings, it was expected with such rushed panic development. XD

## Screenshot

Here’s a screenshot of the old game version I submitted to the game jam (version 1.2):

<a href='/assets/screenshots/lazarr_1_2.jpg'>
	<img class="screenshot" src="/assets/screenshots/lazarr_1_2_small.jpg" alt="Screenshot of Lazarr! version 1.2" width="600" height="400" >
</a>
