---
title: "Web Graveyard"
date: 2023-03-20T02:51:00+01:00
showdate: true
satire: true
usestoc: true
---

## Welcome to the Graveyard
Welcome to the Graveyard of the World Wide Web! We don’t come here to mourn, but to remember of ye horrible olden days in which terrible proprietary data demons, horrible BLOBs and technologies from the underworld terrorized us all. May they all rest in bits.

This place shall be a reminder to us all how Evil can corrupt us all when we stop to pay attention. Let us learn from the past to avoid repeating it.

## Adobe Flash Player

Terrible were the days in which Adobe Flesh Eater, excuse me, **Adobe *Flash Player*** reared its ugly head all over the web till 2020. No WWWanderer could wander the Web for long without encountering a web page infected with this terrible beast.

Adobe Flash Player, or Flash for short, is one of the most evil beasts that have walked the Web ever. And it is all our fault.

Flash is classified as “plugin beast” that was brought to unlife by mad computer scientists of the Adobe corporation. They decided forego the noble profession of computer science by using their skills for evil. Their goal? Infect every website with Flash, so that no website could escape the clutches of the corporation. And once every website depended on Flash, they achieved their true evil goal: Complete WWW Domination.

Their strategy is exactly as you would expect from true supervillains: Mind-control. They told everyone how Flash is actually about fun animations and games. It worked: The whole WWW listened to these siren songs. We were so foolish, and we have walked right into their trap. It didn’t take long and the whole WWW was infected. It has been practically impossible to browse the web and not encounter a Flash-infected site.

The worst of it? We *loved* it. We were ignorant or unaware of the dark and twisted side of Flash.

A particularly nasty capability of Flash is to place something they called “local shared objects” on the victim’s computer. This is Adobe’s euphemism for data leeches, little critters that suck out all of the data from your harddrive like mosquitoes suck blood. Chances are, the victim doesn’t even know they exist. It’s the perfect data heist.

Yet even evil geniuses make mistakes ... They created a beast that they could not control, the beast had a mind of its own. It would frequently rebel and shut itself down by crashing. In its own twisted mind, it has spawned hundreds of terrible bugs that crawl all over the computers and under your skin. Bugs so terrible they would spawn security holes so large, every cracker could happily walk into your system files. Our security databases are full of evidence of the terror of Flash remote-code executions.

Adobe continues to insist all these bugs were *never* intended. Even if we are charitable towards supervillains (and we shouldn’t), it’s hard to deny the true goal with Flash was WWW Domination.

But word spread around, and people slowly realized that maybe inviting a beast into your computer that will happily open a remote vulnerability every few weeks is actually a terrible idea. “But the fun games!”, we said. “But the fun animations!”, we said. We were fools. Flash’s terrible mind control capabilities were strong.

At the end, we were stronger. More and more people had enough of it all, and burned Flash wherever they found it. Eventually, Flash was seen in less and less sites. When major websites and even web browsers decided to drop support, the fate of Flash was sealed.

The supervillains at Adobe desperately tried to keep it on unlife, but it didn’t help. We have found Flash’s weakness: Flash lives of our attention. Flash cannot do anything if you don’t actively invite it into your computer. To defeat Flash, you have to reject its siren songs. Once the web collectively decided to abandon Flash, we have slowly defeated it.

We won. At the end, Adobe declared defeat and decided to officially kill Flash once and for all, the beast it once created. It has dropped all official (un)life support. Not only that: They’ve built a kill-switch into the latest mutations (which they deceptively call “releases”) of Flash to prevent it from running on a future date. Or at least that’s what they *claim* they did …

Flash is not entirely dead. About 1.5% of websites still have a Flash infection. But it remains dormant, thanks to the browsers refusal to cooperate with the beast.

Let us Flash be a reminder that Evil can not prevail forever. Let us never fall again to the siren songs of proprietary data-leeches. Flash only ever became so strong because we fed the beast. Once we stopped feeding the beast, it slowly died.

Adobe Flash Player, rest in bits.


* Status: Mostly dead
* Classification: Plugin beast
* Time of death: 31.12.2020
* Strategy: Lure in its victims with funny animations and games, then strike when they least expect it
* Weapons of choice: Mind-control, remote-code executions, data leeches


## Microsoft Silverlight

History always occurs twice: First as tragegy, then as farce. Microsoft’s mad computer scientists wanted to mimic the questionable “success” of Flash for their own evil goals by spreading their very own abomination across every webpage of the World Wide Web. They too wanted to take a slice of the pie.

Thankfully, like Flash, Silverlight’s biggest weakness is that it feeds from our desires. Since almost nobody paid attention to it, Silverlight never became powerful. Almost nobody bought into Microsoft’s marketing or even cared.

My personal theory is, the siren songs of Flash were far more alluring than Silverlight could ever be. And people never understood the point of yet another browser plugin.

According to stats, Silverlight never got anywhere close to relevancy. And that’s a good thing.

Silverlight is dead because it was never alive. We may never know what terrible horrors this abomination would have unleashed on the WWWanderers, and it’s probably for the best. Sometimes, ignorance *is* bliss.

Microsoft Silverlight, rest in bits.


* Status: Extra-dead (it never lived to begin with)
* Classification: Plugin beast
* Time of death: 2021
* Strategy: Unknown
* Weapon of choice: Unknown


## Java applets

Java applets are another type of browser plugin beast, but unlike Flash or Silverlight, they were actually not created with an evil intention in mind.

A Java applet is an exceptionally unpredictable kind of plugin beast. At one day, it would obey the commands of its programmer perfectly, at another day, it would randomly spawn bug creatures, similar to those from Flash, inviting all evil crackers right to your system files.

We became fed up by this for a very similar reason why they became fed up with Flash: Security risk. So we made the adult decision to do the right thing, which is to collectively kill Java applets of the WWW without mercy. The browsers ultimately came to our rescue.

Java applets had a siren song of its own, but it is far weaker: “I’ll allow you to create something useful, maybe.” Few people today mourn for the days of the Java applet beasts.

Java applets, rest in bits.


* Status: Dead
* Classification: Plugin beast
* Time of death: 2017
* Strategy: Unknown, it has an unpredictable mind of its own.
* Weapon of choice: Spawning terrible bug creatures opening extradimensional portals right into your system files


## &lt;blink&gt;

&lt;blink&gt; was a markup monster spawned by the evil geniuses of Microsoft Corporation. In theory, it is very simple: It makes a portion of a webpage flash on and off.

Sounds simple and fun, right? WRONG! The *true* reason this creature was spawned is none other than for create an army of drones to secretly serve the corporation. &lt;blink&gt;’s *true* purpose is to hypnotize web developers into adopting Microsoft ideology.

It was spawned into unlife in 1994, one year before the terrible First Browser War between Netscape Navigator and Internet Explorer began. Microsoft was threatened by the existence of Netscape, so they needed a plan: One of the Microsoft employees shall infiltrate the team of Netscape to add blinking texts into their browser. Nobody suspected anything. The operation went smoothly.

This is how it works: When an user visits a webpage infected by &lt;blink&gt;, at first the text will blink on. Immediately, the mind of the victim goes into a trance state. It blinks off. The following telepathic message is then transmitted into the victim’s brain:

> THERE IS NO BROWSER BUT INTERNET EXPLORER.  
> YOU SHALL NOT CARE ABOUT INTEROPERABILITY.  
> WEB STANDARDS ARE FOR THE WEAK.  
> PLEDGE ALLEGIANCE TO BILL GATES, OUR ETERNAL LORD AND SAVIOR.

This message is repeated rapidly 666 times within 1 second. During this time, the victim had a brief empty stare. It will blink on again for a final time. The mind now has been successfully manipulated, the user now has become a mindless slave to Microsoft. And worst of all: If the user happened to be a web developer, they too will now spread &lt;blink&gt; all over the web, for other minds to be infected.

The operation was a complete success. In the years that followed, more and more people started adopting the browser from hell, Internet Explorer. Microsoft achieved almost complete WWW domination and won the First Browser War. It was a terrible time for any WWWanderer. I still remember it as if it was yesterday.

Thankfully, the effect of &lt;blink&gt; is time-limited. Eventually, people have woken up from their trance, especially, after users kept complaining about the annoyance of blinking text. Around 2013, all of the big browsers have finally stopped obeying &lt;blink&gt;’s command which rapidly lead to its death.

It has also been theorized this markup monster was *actually* spawned to murder epileptic people. The people responsible for the monster have neither confirmed nor denied this claim.

&lt;blink&gt;, rest in bits.


* Status: Dead
* Classification: Markup monster
* Time of death: 2013
* Strategy: Plaster it all over the web so it is seen by as many potential victims as possible
* Weapon of choice: Hypnotization


## Bitmaps

Bitmaps are one of the lesser pests that the WWWanderer might encounter.

Bitmaps, or `*.BMP` are mostly harmless, but still a pain to deal with. Bitmaps are completely mindless and only have one capability: To grow as large as possible to clog the data tubes. While this is not directly harmful in itself, it was still an annoying pest the WWWanderer and developers have to deal with.

Bitmaps are really strange. They’re part file, part organism. Whenever you’re not paying attention, they *slowly* grow a few bytes larger. You could *swear* that file wasn’t *that* large a few months ago. Bitmaps are harmless *most* of the time, but the longer you wait with dealing with them, the uglier things will be.

Bitmaps usually are the most ugly and horrifying things. Looking at a Bitmap makes people scream in horror, so hideous are these creatures.

Most bitmaps are spawned into existence by inexperienced web developers who didn’t get the memo that bitmaps shouldn’t be unleashed into the Web, except for trolling. The greatest producer of bitmaps is `PAINT.EXE`.

Thankfully, dealing with bitmaps is easy: The most popular method to deal with them is by squishing it in compressors. This turns the unwieldy Bitmap into a nice, little files, like PNG or even better, JPEG, so they no longer clog up the tubes. But in case of an exceptionally ugly abomination, you only have one choice: Kill it with fire!

Most people in the Web have gotten the memo by now and the bitmaps have become a rare sight.

That’s good news, right? **WRONG!** Bitmaps might have mostly died in the web, but only to give birth to something much worse: Two gibibytes worth of autoplaying video on the homepage. But that’s a story for another time …


* Status: Dead
* Classification: BLOB
* Time of death: N/A
* Strategy: None. It is mindless
* Weapon of choice: None, it just grows and grows
