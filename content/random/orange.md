---
title: "Orange Creations"
date: 2023-03-26
showdate: true
---

<p>This page is just for fun. I’m obsessed with the color orange. Here are some of the things I made that are orange.</p>

<h2>Luanti</h2>

<h3>JT2: Temple of Orange</h3>

<a href="/assets/screenshots/jt2_temple_of_orange.jpg">
<img class="screenshot" src="/assets/screenshots/jt2_temple_of_orange_small.jpg" width=600 height=400 alt="Screenshot of a building Luanti server JT2. It shows the interior of a temple made out of orange wool. There are 3 staircase. Two at the left and right side going upwards, one in the middle going downwards. The middle staircase leads to a tree which has lots of oranges in its crown. In front of the tree, some pedestals showing. At the ceiling, there are many orange balloons. At the left and right, there are windows made out of obsidian glass.">
</a>

<p>I’ve built the Temple of Orange for fun in the <a href="https://jt2.intraversal.net/">JT2</a> server in <a href="https://minetest.net">Luanti</a> at coordinates (130, 9, -530). This is a building made out of orange wool. The tree in the middle is surrounded by bronze pedestals showing off various orange items available in the game.</p>

<p>My goal here was to show off every orange item in the game. And I succeeded! I’m quite proud of this building.</p>

<p>These images were taken on 15/01/2022.</p>

<br class="imgclear">

<a href="/assets/screenshots/jt2_temple_of_orange_outside.jpg">
<img class="screenshot" src="/assets/screenshots/jt2_temple_of_orange_outside_small.jpg" width=300 height=200 alt="Screenshot of the entrance of the Temple of Orange. The entrance has a wooden double door and is adorned with an arch made out of orange wool. Two orange trees are to the left and right side. The rooftop is decorated with many oranges and orange balloons.">
</a>

<a href="/assets/screenshots/jt2_temple_of_orange_roof.jpg">
<img class="screenshot" src="/assets/screenshots/jt2_temple_of_orange_roof_small.jpg" width=300 height=200 alt="Screenshot of the roof of the Temple of Orange, taken at night. It’s a flat orange woolen surface, with many oranges on the ground. In the middle, there are orange wool benches. In the background, orange ballons are floating. To the left and right, iron lanterns illuminate the roof.">
</a>

<h3>mtPlace: It spreads!</h3>

<a href="/assets/screenshots/mtPlace_orange_26_03_2023.png">
<img class="screenshot" src="/assets/screenshots/mtPlace_orange_26_03_2023_small.jpg" width=600 height=400 alt="Screenshot of Luanti mtPlace server. It shows a strange orange blob creature with spreading tentacles. Below is the text “It spreads!” is written.">
</a>

<p>mtPlace was a Luanti server in which you have a flat canvas of 512×512 pixels. Players can color each pixels by choosing one of 24 colors.</p>

<p>What is shown here is my attempt to “corrupt” the canvas by spreading orange as far as possible.</p>

<p>This image was taken at 26/03/2023.</p>

<h2>Me & My Shadow</h2>

<h3>Orange theme</h3>

<a href="/assets/screenshots/mams_orange_theme.jpg">
<img class="screenshot" src="/assets/screenshots/mams_orange_theme_small.jpg" width=600 height=400 alt="Screenshot of theme “Orange” for Me & My Shadow">
</a>

<p>A humorous theme I created for <a href="https://acmepjz.github.io/meandmyshadow/">Me & My Shadow</a> in 2012. I posted it <a href="https://forum.freegamedev.net/viewtopic.php?f=48&t=3169">here</a>.</p>
