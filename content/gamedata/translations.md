---
title: "Software translations"
date: 2020-10-27T13:30:00+01:00
---
A big part of my contributions to free software is translating things, especially games, into German. Here’s a list of things I helped translating so far (my own games are not listed):

## Major involvement

* Luanti
* Luanti server “A.E.S.”
* Hedgewars
* Naev
* Dust Racing 2D
* The Butterfly Effect
* OpenTTD
* OpenRCT2
* FreeCol
* FLARE
* Cataclysm: Dark Days Ahead (no longer active)
* The Battle for Wesnoth

## Minor involvement

* 0 A.D.
* Stunt Rally
* SuperTux
* SuperTuxKart
* Xonotic
* Pioneer
* OpenSurge
* Voxelands
* Widelands
* (other games …)

## Non-games

* HexChat
* Nheko
* Blockbench
* Forgejo
* Tenacity
* LMMS
