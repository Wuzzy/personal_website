---
title: "Frequently Asked Questions"
date: 2024-11-29T07:15:50+00:00
---
These are questions people have asked me many times.

## **Q**: “Are you the original author of MineClone 2 / VoxeLibre?”

**A**: Yes.

*However*, I’m no longer active. I have given control over this project to other developers many years ago.

## **Q**: “Can you help me about some Luanti-related problem I have?”

**A**: If it’s related to one of my own Luanti-related creations, then probably yes. Feel free to contact me.

For general support questions about Luanti overall, the answer is no. I don’t want to answer them in private. Please post these questions in the <a href="https://forum.luanti.org/index.php" title="Luanti Forums">Luanti Forums</a>, so that everyone may benefit from the answers.

## **Q**: “I found a bug in one of your games/mods/softwares! What do I do?”

**A**: Preferably you file a so-called issue (i.e. bug report or feature request) in the issue tracker. Most of my code is hosted on <a href="https://codeberg.org" name="Codeberg">Codeberg</a>. My <a href="https://codeberg.org/Wuzzy" title="Wuzzy’s Codeberg profile">Codeberg profile</a> has an overview of all my code. However, you first need to register on Codeberg.

Alternatively, you can reach out to me by one of my contact methods.

## **Q**: “Does your name come from Fuzzy Wuzzy the bear?”

**A**: No.


