---
title: "Copying"
date: 2023-03-20T19:28:00+01:00
---

This website is released under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/), as far as is legally possible. Exceptions and further credits are listed below.

## Text

All written text is written by Wuzzy (except for quotations, obviously) and is released under the {{< license "CC0" >}}.

## Images

The following conditions apply for images:

* The Wuzzy “W”: {{< license "CC0" >}}
* Pride flags: {{< license "CC0" >}} (these are too trivial for copyright)
* Nose Ears / Nasenohren logo: {{< license "CC BY-SA 3.0" >}}, created by Wuzzy using the “Nina” font by Nina Paley

## Screenshots

The screenshots of Glitch and Shadow Forest are under {{< license "CC0" >}}.

For all other screenshots I claim fair use. I did take the screenshots myself, but they show media created by other people. Obviously, because these are screenshots, they are technically transformative works of the original artwork files.

The authors and the respective license of the depicted media is listed below:

### Games

* Repixture: Shows off artwork by [minetest.net](https://minetest.net) user Kaadmy (License: {{< license "CC BY-SA 4.0" >}})
* MineClone 2: Shows off artwork by XSSheep (License: {{< license "CC BY-SA 3.0" >}})
* Hades Revisited: Credit for artwork goes to Glünngi and Minetest Game contributors (License: {{< license "CC BY-SA 3.0" >}})
* JT2 and Catlandia servers: Credit for artwork goes to Minetest Game contributors (License: {{< license "CC BY-SA 3.0" >}})
* mtPlace server: Due to the chaotic nature of this server, crediting does not make sense here
* Orange theme for Me & My Shadow: Co-authors of the theme are: Wuzzy, Tedium, Vera Kratochvil, Chris S and Daniel Gregory Beno (License: {{< license "CC BY-SA 3.0" >}})
* Hedgewars: Credit for artwork goes to Hedgewars contributors (License: [GNU FDL 1.2](https://www.gnu.org/licenses/old-licenses/fdl-1.2.html). See [hedgewars.org](https://hedgewars.org) for details
* Stunt Rally: The tracks shown were created by Wuzzy, but not the artwork. For info about artwork, see <https://github.com/stuntrally/stuntrally>

### Luanti mods

* Rail Corridors: Credit for artwork goes to Minetest Game contributors (License: {{< license "CC BY-SA 3.0" >}})
* Pyramids: Credit for artwork goes to Minetest Game contributors (License: {{< license "CC BY-SA 3.0" >}})
* Flying Carpet: Carpet texture by Roman Zacharij (License: [MIT License](https://mit-license.org/)), everything else by Minetest Game contributors. (License: {{< license "CC BY-SA 3.0" >}})
* Easy Vending Machines: Vending machine textures by Wuzzy (under {{< license "CC0" >}}) credit for the other artwork goes to Minetest Game contributors (License: {{< license "CC BY-SA 3.0" >}})
* HUD Bars: The hotbar, sapling, heart and breath icons are images from Minetest Game, created by Minetest Game contributors. The same applies for the depicted game scene. Diamond leggings and diamond shield textures by davidthecreator (License: {{< license "CC BY-SA 3.0" >}})
* Help modpack: Under {{< license "CC0" >}}
* Schematics Editor: Under {{< license "CC0" >}}
* Perlin Explorer: Under {{< license "CC0" >}}
* L-System Tree Utility: The foreground shows a trivial user interface. Background shows a Minetest Game scene. The credit for artwork of Minetest Game goes to Minetest Game contributors (License: {{< license "CC BY-SA 3.0" >}})
* TNTTag: Credit for artwork goes to Zughy for the Soothing32 texture pack (License: {{< license "CC BY-SA 4.0" >}})
* Tntrun: Credit for artwork goes to Zughy for the Soothing32 texture pack (License: {{< license "CC BY-SA 4.0" >}})
* X-Decor-libre: Credit for artwork goes to Gambit, kilbith and Cisoun ({{< license "CC0" >}})
