---
title: "Privacy Policy"
date: 2019-11-22T07:20:00+01:00
---
Wuzzy, the author and publisher of this website does not collect or store any personal identifiable information. However, the server operator (who is not Wuzzy) _might_ collect personal identifiable information (such as the IP address).

If you contact me, treat it as if we were in a *public* chatroom. Please be aware our communication can be read and tracked by many people along the way. This is the Internet, after all. While I won’t track you, I can’t promise that *other* people won’t track you. So please do not send highly-sensitive information to me, I’m not an investigative journalist.
