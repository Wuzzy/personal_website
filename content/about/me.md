---
title: "About me"
date: 2023-03-19T18:53:00+01:00
---
<img class="logo" src="/assets/images/Wuzzy_avatar_nonsquare.png" width="150" height="117" alt="Wuzzy’s avatar">

My name is **Wuzzy**, I like the color orange, video games, free software and the concept of absurdity. I speak English and German.

Moreover, I like freedom, satire, movies, software development, the Internet and making sound recordings. I dislike oppression, anti-intellectualism, competitive sports, being sick, death, surveillance, having sex and romantic comedies.

## Identity

* Preferred name: **Wuzzy**
	* Pronunciation: [ˈwuˌzi]
* Full name\*: Wuzzy Wuzzard
	* Pronunciation: [ˈwuˌzi ˈwuˌzard]
* Pronoun: “he”
* Gender: Yes
* Orientation: Aro-ace (aromantic asexual)

----

\* = I only use this name when absolutely needed, like for disambiguation.

## What I like

I like the color **orange**. Maybe I am a little bit [obsessed](/random/orange) by it.

I like **playing video games** and did so since I was little. I have “completed” playing over 200 video games, meaning I reached the end of over 200 games. Games that have no end don’t count. I prefer singleplayer and I rarely play competitively. I play games of various genres: Action, shooter, strategy, sandbox, puzzle, RPG, adventure, arcade, and more. The exact list of games I play and have played is secret.

I like **[making video games](/games/games)** and creating [stuff](/gamedata/overview) for them. I’ve started or continued development of a few video games myself, and even won a game jam once. I have been coding for decades now.

I am both a huge fan and creator of **free software** and surround myself with it every day. I try to avoid non-free software as much as possible. My operating system is Parabola GNU/Linux-libre. I regularly criticize those who want to force non-free software into my workflow. [Here’s a list of software I use](/about/software).

My webcomic “[Nose Ears](/comics/nose_ears)” is my way of dealing with the absurd. The world has no shortage of bad arguments, hilarious fallacies and weird bullshit to make fun of.

I like being **alone**. I *can* deal with other people, but I love my alone time more.

When I’m outside, I tend to take **photos** or make **sound recordings**. I have built a large collection of photos and sound recordings. A few sounds even made their way into may games. I use LS-10 to record sounds.

I’m *really* not into sports, with one rare exception: swimming.

## My stances

### Freedom

I believe **freedom** is the most important thing you can have in our way-too-short life. Most of my beliefs about the world are derived from this core principle.

This freedom has one core limitation: Once your attack the freedom of someone else, your freedom ends. I have no patience for people defending their bigotry with “their freedom”. For example, if you advocate for genocide and defend yourselves with your “religious freedom”, you’re full of shit.

I am strongly for **free software**, i.e. software that is *not* attacking your fundamental freedoms as an user: The freedoms to use it, sharing it, modifying it, and sharing modifications of it. My dream is that one day, free software is the default. Free software should be the expected norm in society so that *proprietary software advocates*, not free software advocates, are the awkward ones who have to defend their position.

My stance on <abbr title="Lesbian, Gay, Bisexual, Biromantic, Trans*, Queer, Questioning, Intersex, Asexual, Aromantic, Agender, Pansexual, Panromantic and others">**LGBTQIAP+**</abbr>: It’s all valid. I consider an LGBTQIAP+ identity to be just part of who you are. Being able to live freely as a LGBTQIAP+ person is essential to freedom. I support queer liberation. As I am aromantic and asexual myself, this is in my own rational self-interest.

Although I am non-religious, I support **religious freedom** as long it’s not being abused to attack the freedom of others. I still take the liberty of brutally mocking dogshit religious arguments. :-)

I am **sex-positive**. This means I support sexual liberation movements, but it *must* be consent-based. Although I can’t relate to people who actually have sex (because I never have sex), I believe the freedom for them to live out their sexuality is critical to freedom overall.

### Oppression

I hate all **systems of oppression**, and they both fascinate and horrify me. Fascinating, because it is an interesting case study of the deep dark depths of human nature. But it’s horrifying when I remember I’m part of society myself. OH SHI—!

I consider the following as systems of oppression:

* Fascist crap, stalinist crap, nazi crap, etc.
* Authoritarianism
* Prison
* Capitalism
* Copyright
* Proprietary software
* Forced religion
* Forced pregnancies
* Various forms of bigotry (queerphobia, sexism, racism, etc.)

### Anti-intellectualism

In my opinion **anti-intellectualism is one of the greatest dangers to humanity**. Anti-intellectualism is embracing the irrational. It is the open rejection of logic and critical thinking in favor of some nonsense like a “higher truth” or “cosmic principles” or other superstitions. In one word, anti-intellectualism is bullshit. Anti-intellectualism allows people to justify literally anything they like, no matter how insultingly wrong it is.

It allows people to ignore the scientific consensus of human-made global heating. It allows people to ignore the truth of evolution. The truth of the existence of LGBTQIAP+ people. Anti-intellectualism is the root of many, many bad things.

This is why irrationality is one of the biggest topics in Nose Ears. IMHO the greatest weapon against anti-intellectualism is brutal but honest mockery.

### Religion

I don’t positively believe in any Gods, higher beings, the supernatural, etc. I **reject all religions**. I’m a fan of science, but I’m not a scientist.

## My orientation explained

I am an out-and-proud **aro-ace**, which is short for “aromantic and asexual”.

<img class="logo" src="/assets/images/asexual_flag.png" width="150" height="90" alt="The asexual flag. 4 horizontal stripes, from top to bottom: black, gray, white, purple" title="Asexual Pride Flag">

I’m **asexual**. I feel no sexual attraction towards anyone ever. And I like it that way. While other people find people “hot”, I never find *nobody* hot. It never happened, and I doubt it ever will. **I do not give a fuck**, literally. :-)

<br class="imgclear">

<img class="logo" src="/assets/images/aromantic_flag.png" width="150" height="90" alt="The aromantic flag. 5 horizontal stripes, from top to bottom: green, light green, white, gray, black" title="Aromantic Pride Flag">

I’m also **aromantic**. I feel no romantic or attraction towards anyone and I don’t desire romantic relationships of any kind. If you want to start a romantic relationship with me, I will run away screaming. The concept of romance is completely alien to me.

<br class="imgclear">

<img class="logo" src="/assets/images/queer_flag.png" width="150" height="90" alt="One of many queer flags. 9 horizontal stripes, from top to bottom: black, light blue, blue, green, white, yellow, red, light red, black" title="a queer pride flag">

I am also **queer**. The word “queer” has a *lot* of definitions, but for me personally it simply means I don’t fit into heteronormative expectations, i.e. the false belief that everyone is straight (or should be). As I’m not straight (I’m an aro-ace), my existence is an **affront to heteronormativity**. I’m queer by implication.

### More identities

Additionally, I am sex-averse, romance-repulsed and aegosexual.

**Romance-repulsed**: I find the thought of me being involved in any romantic relationship repulsive. Marriage horrifies me. I do not want to have anything to do with any romance whatsoever. I hate romantic comedies with a deep passion. I tend to make fun of the concept of love.

**Sex-averse**: I am also highly averse to sex, but not as much as I am to romance. If you want to seduce me, you will fail.

**Aegosexual**: I am fine with the thought of sex as a concept, I even kinda enjoy fantasizing about it, but as soon I am supposed to have sex myselves, it’s all over. *Thinking* about sex and actually having sex are two very different things.

I never ever had sex nor kissed and I don’t have any desire to do so. And it’s *fine*. My solution to my repulsion/aversion to romance and sex is simple: I remove myselves from these things.

To some people, having no sexual or romantic desire or attraction might sound sad. But I’m perfectly happy with these identities and wouldn’t trade them away. *Before* I realized I was aro-ace, I actually felt a bit *worse* because I thought I was broken for having no sexual desire. But then I realized that’s bullshit and started to embrace my true self.

## Other

* [Me in the Internet](/about/internet)
* [The software I use](/about/software)
