---
title: "The Software I use"
date: 2023-03-20T14:41:00+01:00
---

Here’s an excerpt of software I use regularly:

* **Operating system**: Parabola GNU/Linux-libre
* **Window system**: XFCE
* **2D Graphics**: GIMP, Inkscape
* **3D Graphics**: Blockbench, Blender
* **Media**: VLC Media Player, mplayer, Ario, MPD
* **Audio editing**: Tenacity
* **Web design**: Hugo
* **Text editing**: Vim, Leafpad, Mousepad
* **Chatting**: Hexchat, Element, Pidgin
* **Browsing**: GNU IceCat
* **E-mail**: Claws Mail
