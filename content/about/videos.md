---
title: "Videos"
date: 2024-03-19T09:06:00+01:00
---

I’m not really a video maker, but I’ve posted some Let’s Play videos at [Wuzzy plays Games](https://tube.tchncs.de/c/wuzzy_plays_games/).
This channel has a Let’s Play of [FreedroidRPG](https://freedroid.org) 1.0.
