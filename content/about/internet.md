---
title: "Me in the Internet"
date: 2019-11-22T07:20:00+01:00
---

## Communities

### Talking

I am active in these communities:

* [Mastodon](https://cyberplace.social/@Wuzzy): I post most of my updates here
* [Luanti Forums](https://forum.minetest.net/)
* [FreeGameDev.net](https://freegamedev.net/)

### Coding

Most of my code is hosted on these code hosting sites:

* [Codeberg.org](https://codeberg.org/Wuzzy)
* [repo.or.cz](https://repo.or.cz/projlist.cgi?name=add48bb6480aece6a06bb70be09d3310) (legacy)

Codeberg.org is where my most of my code is hosted.

I also have accounts on GitLab (as “Wuzzy”) and GitHub (as “Wuzzy2”) but only to contribute and comment to other projects. [I recommend to move away from GitHub](/essays/github).

### Other

My website profile on Neocities for [Nose Ears](/comics/nose_ears):

* [Neocities](https://neocities.org/site/wuzzy)

### Direct contact

See [Contact](/meta/contact).

## Create

I’ve created numerous things and posted them on the Internet:

* [Games](/games/games)
* [Game Contributions](/gamedata/overview).
* [Comics](/comics/nose_ears)
* [Let’s Play videos](/about/videos)

### Other

I’ve coined at least [597 words](https://jbovlaste.lojban.org/personal/Wuzzy?extra=valsi) for the artificial language of Lojban, but I haven’t been active since years.

Platforms on which I was rarely active include:

* German Wikimedia projects (as “Wiki-Wuzzy”):
    * Wikidata: rare
    * Wikipedia: very rare
    * Wikimedia Commons: very rare
* Openclipart: rare
